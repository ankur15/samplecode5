﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo5
{
    class Program
    {
        static void Main(string[] args)
        {
			//IHairCut permAndCut = new Perm(new RegularHairCut());
			//IHairCut haircur = new RegularHairCut();

			//Console.WriteLine("Services");
			//Console.WriteLine("----------------Regular Hair Cut-----------------");
			//Console.WriteLine(haircur.getDescription());
			//Console.WriteLine("Price: $" + haircur.getCost());


			//Console.WriteLine("----------------Regular & Perm Hair Cut-----------------");
			//Console.WriteLine(permAndCut.getDescription());
			//Console.WriteLine("Price: $" + permAndCut.getCost());
			//Console.ReadKey();

			// We can have completely different calculations performed
			// on completely different objects just by defining a
			// new concrete Visitor

			YearlyBonusVisitor yearlyBonusCalculator = new YearlyBonusVisitor();

			SalesTrainee bradTrainee = new SalesTrainee(5, 1, 20000);
			Salesman tomSalesman = new Salesman(150000, 62);
			Boss rossBoss = new Boss(1000000, 1200, 4000000);

			Console.WriteLine("YEARLY BONUS");

			Console.WriteLine(bradTrainee.accept(yearlyBonusCalculator));
			Console.WriteLine(tomSalesman.accept(yearlyBonusCalculator));
			Console.WriteLine(rossBoss.accept(yearlyBonusCalculator));

			// Calculate Quarterly Bonus

			QuarterlyBonusVisitor quarterlyBonusCalculator = new QuarterlyBonusVisitor();

			bradTrainee = new SalesTrainee(1, 0, 20000);
			tomSalesman = new Salesman(30000, 22);
			rossBoss = new Boss(200000, 300, 11000);

			Console.WriteLine("\nQUARTERLY BONUS");

			Console.WriteLine(bradTrainee.accept(quarterlyBonusCalculator));
			Console.WriteLine(tomSalesman.accept(quarterlyBonusCalculator));
			Console.WriteLine(rossBoss.accept(quarterlyBonusCalculator));
			Console.ReadKey();
		}
    }
}
