﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo5
{
	public class Perm : HairCutDecorator
	{

		public Perm(IHairCut haircut) : base(haircut)
		{
		}


		public override String getDescription()
		{
			return hairCut.getDescription() + "\nAdd Chemicals and Put Hair in Rollers";
		}


		public override double getCost()
		{
			return hairCut.getCost() + 75.00;
		}


	}
}
