﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo5
{
    public interface IHairCut
    {
        String getDescription();
         double getCost();
    }
}
