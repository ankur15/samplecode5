﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo5
{
	public class Boss : IVisitable
	{


	private double totalSalesAmount;
	private int newCustomers;
	private double officeExpenses;

	public Boss(double totalSalesAmount, int newCustomers, double officeExpenses)
	{
		this.totalSalesAmount = totalSalesAmount;
		this.newCustomers = newCustomers;
		this.officeExpenses = officeExpenses;
	}

	public double accept(IVisitor visitor)
	{

		return visitor.visit(this);

	}

	// Getters & Setters

	public double getTotalSalesAmount()
	{
		return totalSalesAmount;
	}
	public void setTotalSalesAmount(double totalSalesAmount)
	{
		this.totalSalesAmount = totalSalesAmount;
	}
	public int getNewCustomers()
	{
		return newCustomers;
	}
	public void setNewCustomers(int newCustomers)
	{
		this.newCustomers = newCustomers;
	}
	public double getOfficeExpenses()
	{
		return officeExpenses;
	}
	public void setOfficeExpenses(double officeExpenses)
	{
		this.officeExpenses = officeExpenses;
	}
}
}
