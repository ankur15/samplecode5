﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo5
{
	public class YearlyBonusVisitor : IVisitor
	{

	public double visit(SalesTrainee trainee)
	{

		Console.WriteLine("Trainees Yearly Bonus");

		// Find bonus percentage using ternary operator

		double yearlyBonusPercentage = (trainee.getSickDays() < 10 && trainee.getFailedTests() < 2) ? .10 : .02;

		double yearlyBonusAmount = trainee.getSalary() * yearlyBonusPercentage;

		return yearlyBonusAmount;

	}

	public double visit(Salesman salesman)
	{

		Console.WriteLine("Salesmans Yearly Bonus");

		double yearlyBonusPercentage = (salesman.getTotalSalesAmount() > 100000 && salesman.getNewCustomers() > 50) ? .12 : .04;

		double yearlyBonusAmount = salesman.getTotalSalesAmount() * yearlyBonusPercentage;

		return yearlyBonusAmount;

	}

	public double visit(Boss boss)
	{

		Console.WriteLine("Bosses Yearly Bonus");

		double yearlyBonusPercentage = (boss.getOfficeExpenses() < 50000 && boss.getNewCustomers() > 1000) ? .15 : .04;

		double yearlyBonusAmount = boss.getTotalSalesAmount() * yearlyBonusPercentage;

		return yearlyBonusAmount;

	}

}
}
