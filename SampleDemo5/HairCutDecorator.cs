﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo5
{
	public abstract class HairCutDecorator : IHairCut
	{


	public IHairCut hairCut;

	// The decorator will be able to dynamically customize
	// HairCuts

	public HairCutDecorator(IHairCut hairCut)
	{

		this.hairCut = hairCut;

	}

	public virtual String getDescription()
	{

		return hairCut.getDescription();

	}

	public virtual double getCost()
	{

		return hairCut.getCost();

	}

}
}
