﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo5
{
	// Each concrete visitor type will implement
	// the Visitor interface. Here we define every
	// instance of method overloading needed

	public interface IVisitor
	{

		 double visit(SalesTrainee trainee);
		double visit(Salesman salesman);
		double visit(Boss boss);

	}
}
